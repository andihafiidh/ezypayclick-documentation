Date.prototype.toInputFormat = function() {
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
    var dd  = this.getDate().toString();
    var h = this.getHours().toString();
    var m = this.getMinutes().toString();
    var s = this.getSeconds().toString();
    return yyyy + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + (dd[1]?dd:"0"+dd[0]) + " " + (h[1]?h:"0"+h[0]) + ":" + (m[1]?m:"0"+m[0]) + ":" + (s[1]?s:"0"+s[0]) ; // padding
};

function getDate(add, tgl = ""){            
    var date = new Date();

    if(tgl){
       date = new Date(tgl); 
    }

    var days = parseInt(add, 10);

    if(!isNaN(date.getTime())){
        date.setDate(date.getDate() + days);                
        return date.toInputFormat();
    } 
    
    console.error("Error : Invalid Date");  
    return;
}

$.fn.EzyPay = function(options) {

    var base_url = 'https://trx.ezypay.id';
    var url = base_url + '/payment';

    var settings = $.extend({                
        pre_id: "TRX",
        id: null,
        merchant: null,
        description: "Deskripsi tagihan merchant",
        amount: null,
        exp_date: null,
        exp_after: 1,
        callback: base_url
    }, options );

    return this.click(function(){

        if($(this).parent().find("#ezypay-click")){
            $("#ezypay-click").remove();
        }

        if(!settings.merchant){
            console.error("Error: missing parameter merchant");
            return;
        }
        
        if(!settings.amount){
            console.error("Error: missing parameter amount");
            return;
        }              


        if(!settings.exp_date){

            settings.exp_date = getDate(settings.exp_after);
            
        }

        if(!settings.id){
            settings.id = String(Date.now()).concat(Math.floor(Math.random() * 100));

            if(settings.pre_id){
                settings.id = settings.pre_id.concat("-", settings.id);
            }  
        }            
        
        

        input = ' <input type="text" name="transaction_id" value="'+settings.id+'"><input type="text" name="description" value="'+settings.description+'"><input type="text" name="merchant" value="'+settings.merchant+'"><input type="text" name="amount" value="'+settings.amount+'"><input type="text" name="exp_date" value="'+settings.exp_date+'"><input type="text" name="callback" value="'+settings.callback+'"><input type="submit" value="create">';
        form = '<form id="ezypay-click" action="'+url+'" method="POST" hidden>' + input + '</form>';

        html = '<!DOCTYPE html>' +
                '<html>' +
                    '<head>' +
                        '<meta charset="utf-8" \/>' +
                        '<meta http-equiv="X-UA-Compatible" content="IE=edge">' +
                        '<title>EzyPay Click Payment<\/title>' +
                        '<meta name="viewport" content="width=device-width, initial-scale=1">' +
                    '<\/head>' +
                    '<body>' + 
                        form + 
                        '<script src="https:\/\/code.jquery.com\/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk\/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"><\/script>' +
                        '<script>' +
                            '$(document).ready(function(){ $("#ezypay-click").submit(); })' +
                        '<\/script>' +
                    '<\/body>' +
                '<\/html>';                                
        
        $("#ezypay-click").submit();
        var w = new window.open(url, "ezypayCheckout");                
        w.document.write(html);
        w.document.close();
    });
};