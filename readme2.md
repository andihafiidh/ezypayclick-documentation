Klik EzyPay
=======
Metode pembayaran online baru bagi Usaha/Toko Online milik Anda.
> Pluging JQuery untuk membuat Tagihan Merchant khusus Member EzyPay.

 * Mudah dalam bertransaksi secara online.
 * Pemasangan tidak rumit.
 * Langsung masuk ke dalam saldo EzyPay milik Anda.
 * Pencairan saldo terjadwal.
 * Tanpa penambahan biaya administrasi bagi Customer Anda.  


Mulai Sekarang
---------------

Untuk dapat memanfaatkan fitur ini maka Anda harus memiliki akun EzyPay, jika belum [daftar sekarang](#daftar) kemudian [ajukan merchant](#pengajuan). Setelah itu [pasang kode program](#kode) menggunakan JavaScript.

###<a name="daftar"></a> Daftar EzyPay

Untuk mendaftar EzyPay silahkan download Aplikasi EzyPay atau daftar melalui [https://trx.ezypay.id][trx]. 
> [Unduh Aplikasi >][playstore]

###<a name="pengajuan"></a> Pengajuan Merchant

Untuk dapat menjadi salah satu Merchant Kami, maka Anda harus melengkapi data *Profile* Anda dan menghubungi *Customer Service EzyPay*.

Anda akan diminta untuk mengirim beberapa data diri diantaranya :

  * Nomor Identitas
  * Nama Merchant
  * Alamat Merchant
  * Akun EzyPay Anda

Setelah proses verifikasi dan disetejui, maka Anda akan mendapatkan Token Merchant yang digunakan saat bertransaksi. 

###<a name="kode"></a> Pemasangan Kode Program
**Langkah Mudah** pemasangan Klik EzyPay :

  **1.** Buat tombol pembayaran.
``` html
  ...
    <button id="btn-payment">Bayar Sekarang</button>
  ...
```
  **2.** Pasang JQuery terlebih dahulu sebelum* `<body>` *website Anda berakhir.
``` html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
```
  **3.** Pasang Klik EzyPay dibawah JQuery tadi, serta ganti TOKEN_MERCHANT_ANDA dengan kode token milik Anda.
``` html
<script src="https://trx.ezypay.id/API/click/js?token=TOKEN_MERCHANT_ANDA"></script>
```
  **4.** Deklarasi kan tombol Klik EzyPay pada JavaScript sesuai kebutuhan Anda.
``` html
<script>
  ...
    $('#btn-payment').EzyPay({        
      id: "ABC-12345",        
      description: "Pembelian barang online",
      amount: 150000,   
      exp_date: "2018-12-12 23:59:00"        
    });
  ...
</script>
```
Metode pembayaran melalui EzyPay sudah siap digunakan.
> [Lihat contoh kode >][example]

Cara Kerja
------------

Metode pembayaran menggunakan saldo EzyPay untuk Usaha/Toko Online ini bekerja dengan cara berikut :

#### Pengiriman informasi tagihan

Dengan mendeklarasikan tombol Klik EzyPay pada JavaScript maka informasi mengenai tagihan seperti id transaksi, jumlah tagihan, deskripsi transaksi, tanggal kadaluarsa tagihan, dan lain lain. Informasi tersebut akan dikirimkan menuju EzyPay.

#### Pengecekan member EzyPay

Setelah informasi tagihan diterima oleh EzyPay, maka halaman baru akan terbuka dan terjadi pengecekan Member EzyPay.

Apabila member belum Login maka akan diarahkan ke halaman Login terlebih dahulu, jika sudah maka akan diarahkan menuju halaman informasi tagihan.

#### Pembuatan tagihan

Setelah informasi tagihan telah dianggap benar oleh Member EzyPay, maka Member akan diarahkan untuk membuat tagihan.

#### Pembayaran tagihan

Ketika tagihan telah dibuat, maka Member diarahkan untuk memasukkan kode PIN transaksi untuk membayar tagihan tersebut.

#### Selesai

Saldo akan dikirim ke Akun EzyPay milik Anda. 

> Anda dapat melihat status tagihan pada menu riwayat transaksi milik Anda.


> atau menggunakan fitur *callback* untuk menghubungkan data transaksi dengan sistem yang Anda miliki. **(Fitur ini masih dalam tahap pengembangan)**

Kode Program
===========

Atribut
-----

### id

Merupakan id transaksi dari Merchant milik Anda, id bersifat unik tidak diperkenankan terdapat transaksi dengan id yang sama.
``` javascript
...
  id:"ABC-12345"
...
```

> Anda dapat menggunakan atribut [`pre_id`](#pre_id) sebagai opsi lain.

###<a name="pre_id"></a> pre_id
Sebagai pengganti attribut `id`. Jika Anda mengalami kesulitan dalam membuat kode unik transaki, Anda dapat memanfaatkan `pre_id` sebagai kode awal id transaksi merchant Anda.

``` javascript
...
  pre_id:"ABC"
...
```
> Dengan adanya atribut ini, maka id transaksi akan otomatis dibuatkan dengan awalan `pre_id` dan dilanjutkan dengan **15 digit angka** acak. 

> contoh: ABC-123456789012345

### description
Sebagai deskripsi dari transaksi pada Tagihan Merchant EzyPay.
``` javascript
  ...
    description: "Pembayaran barang online"
  ...
```

### amount
Jumlah nominal saldo yang akan di tagihkan kepada Member EzyPay.

``` javascript
  ...
    amount: 150000
  ...
```

### exp_date
Merupakan batas waktu tagihan dapat dibayar, apabila melebihi batas waktu tersebut tagihan belum dibayar maka Tagihan akan dibatalkan.

``` javascript
  ...
    exp_date: "2018-12-12 23:59:00"
  ...
```

> Format waktu yang digunakan adalah "YYYY-MM-DD H:i:s"

> Dapat digantikan oleh atribut [`exp_after`](#exp_after)

###<a name="exp_after"></a> exp_after
Sebagai pengganti dari atribut `exp_date`, atribut ini akan membuat batas waktu pembayaran tagihan dalam selang waktu beberapa hari.

``` javascript
  ...
    exp_after: 1
  ...
```

> Angka **1** berarti batas waktu pembayaran tagihan selama **satu hari** sesuai dengan kelipatan harian.

Acknowledgements
====

© 2018, PT. Arta Elektronik Indonesia

> Jika masih terdapat pertanyaan silahkan hubungi Customer Service.

**Klik EzyPay** is authored and maintained by [EzyPay][ezy].

[ezy]: http://ezypay.id
[trx]: https://trx.ezypay.id

[playstore-btn]: http://klik.ezypay.id/google-play-badge.png
[playstore]: https://play.google.com/store/apps/details?id=id.ezypay.android&hl=en

[example]: http://klik.ezypay.id/click.php

[dist]: https://trx.ezypay.id/API/click/js
